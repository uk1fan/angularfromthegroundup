(function(app){
    var NgModule = ng.core.NgModule;
    var BrowserModule = ng.platformBrowser.BrowserModule;
    var platformBrowserDynamic = ng.platformBrowserDynamic.platformBrowserDynamic;
    var AppComponent = app.AppComponent;
    var QuoteService = app.QuoteService;
    var RandomQuoteComponent = app.RandomQuoteComponent;

    app.AppModule = NgModule({
        imports: [BrowserModule],
        declarations: [AppComponent, RandomQuoteComponent],
        providers: [{provide: QuoteService, useClass: QuoteService}],
        bootstrap: [AppComponent]
    })
    .Class({
        constructor: function() { }
    });

})(window.app || (window.app = {}));